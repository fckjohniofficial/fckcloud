package eu.sentinal.fckcloudlib.netty.packet.defaults;

import eu.sentinal.fckcloudlib.netty.packet.Packet;
import io.netty.buffer.ByteBuf;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

public class SetNamePacket extends Packet {

    private String name = "";

    public SetNamePacket(String name) {
        this.name = name;
    }

    public SetNamePacket() {
    }

    @Override
    public void read( ByteBuf byteBuf ) throws IOException {
        int length = byteBuf.readInt();
        byte[] bytes = new byte[length];
        for ( int i = 0; i < length; i++ ) {
            bytes[i] = byteBuf.readByte();
        }
        this.setName( new String( bytes ) );
    }

    @Override
    public void write( ByteBuf byteBuf ) throws IOException {
        byte[] bytes = this.getName().getBytes( StandardCharsets.UTF_8 );
        byteBuf.writeInt( bytes.length );
        byteBuf.writeBytes( bytes );
    }

    @Override
    public String toString() {
        return "SetNamePacket{" +
                "name='" + name + '\'' +
                ", uniqueId=" + uniqueId +
                '}';
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
