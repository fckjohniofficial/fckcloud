package eu.sentinal.fckcloudlib.netty.packets;

import eu.sentinal.fckcloudlib.netty.packet.Packet;
import eu.sentinal.fckcloudlib.server.ServerData;
import io.netty.buffer.ByteBuf;

public class UnregisterServerPacket extends Packet {

    private ServerData serverData;

    public UnregisterServerPacket( ServerData serverData ) {
        this.serverData = serverData;
    }

    @Override
    public void read( ByteBuf byteBuf ) throws Exception {
        this.serverData = new ServerData();
        this.getServerData().fromByteBuf( byteBuf );
    }

    @Override
    public void write( ByteBuf byteBuf ) throws Exception {
        this.getServerData().toByteBuf( byteBuf );
    }

    @Override
    public String toString() {
        return "UnregisterServerPacket{" +
                "serverData=" + serverData +
                ", uniqueId=" + uniqueId +
                '}';
    }

    public ServerData getServerData() {
        return this.serverData;
    }
}
