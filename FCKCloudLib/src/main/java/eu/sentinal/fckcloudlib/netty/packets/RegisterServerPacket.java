package eu.sentinal.fckcloudlib.netty.packets;

import eu.sentinal.fckcloudlib.netty.packet.Packet;
import eu.sentinal.fckcloudlib.server.ServerData;
import io.netty.buffer.ByteBuf;

public class RegisterServerPacket extends Packet {

    private ServerData serverData;

    /**
     * Will be sent from CloudMaster to BungeeCord
     * to register a Server
     */
    public RegisterServerPacket( ServerData serverData ) {
        this.serverData = serverData;
    }

    @Override
    public void read( ByteBuf byteBuf ) throws Exception {
        this.serverData = new ServerData();
        this.getServerData().fromByteBuf( byteBuf );
    }

    @Override
    public void write( ByteBuf byteBuf ) throws Exception {
        this.getServerData().toByteBuf( byteBuf );
    }

    @Override
    public String toString() {
        return "RegisterServerPacket{" +
                "serverData=" + serverData +
                ", uniqueId=" + uniqueId +
                '}';
    }

    public ServerData getServerData() {
        return this.serverData;
    }
}
