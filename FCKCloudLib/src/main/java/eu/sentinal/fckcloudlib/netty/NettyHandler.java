package eu.sentinal.fckcloudlib.netty;

import eu.sentinal.fckcloudlib.netty.client.NettyClient;
import eu.sentinal.fckcloudlib.netty.packet.Packet;
import eu.sentinal.fckcloudlib.netty.server.NettyServer;
import io.netty.channel.Channel;

import java.util.*;
import java.util.function.Consumer;

public class NettyHandler {

    public static boolean DEBUGMODE = false;

    private static List<PacketHandler> packetHandlers = new ArrayList<>();

    private static List<ConnectionListener> connectionListeners = new ArrayList<>();

    private static Map<String, Channel> clients = new LinkedHashMap<>();

    private static NettyHandler instance;

    private Map<UUID, ArrayList<Consumer<Packet>>> packetCallbacks = new HashMap<>();

    private PacketHandler handler;

    private Type type = Type.CLIENT;

    private NettyClient nettyClient;

    private NettyServer nettyServer;

    public NettyHandler() {
        NettyHandler.instance = this;

        Runtime.getRuntime().addShutdownHook( new Thread(() -> {
            if ( NettyHandler.this.getNettyClient() != null ) {
                NettyHandler.this.getNettyClient().disconnect();
            }
            if ( NettyHandler.this.getNettyServer() != null ) {
                NettyHandler.this.getNettyServer().stopServer();
            }
        }) );
    }

    public static List<PacketHandler> getPacketHandlers() {
        return NettyHandler.packetHandlers;
    }

    public static List<ConnectionListener> getConnectionListeners() {
        return NettyHandler.connectionListeners;
    }

    public static Map<String, Channel> getClients() {
        return NettyHandler.clients;
    }

    public static NettyHandler getInstance() {
        return NettyHandler.instance;
    }

    public void reconnectToServer( Consumer<Boolean> consumer ) {
        if ( this.getNettyClient() == null ) {
            return;
        }

        this.getNettyClient().reconnect();
    }

    public void reconnectToServer( int delaySeconds, Consumer<Boolean> consumer ) {
        if ( this.getNettyClient() == null ) {
            return;
        }

        this.getNettyClient().scheduleConnect( delaySeconds * 1000 );
    }

    public void connectToServer( String host, int port, Consumer<Boolean> consumer ) {
        this.type = Type.CLIENT;

        this.unregisterAllPacketHandler();
        this.unregisterAllConnectionListener();

        // Close Existing Server Connection
        if ( this.getNettyServer() != null ) {
            this.getNettyServer().stopServer();
        }

        // Close Existing Client Connection
        if ( this.getNettyClient() != null ) {
            this.getNettyClient().disconnect();
        }


        this.nettyClient = new NettyClient();
        this.nettyClient.connect( host, port, consumer );
    }

    public void startServer( int port, Consumer<Boolean> consumer ) {
        this.type = Type.SERVER;

        this.unregisterAllPacketHandler();
        this.unregisterAllConnectionListener();

        // Close Existing Server Connection
        if ( this.getNettyServer() != null ) {
            this.getNettyServer().stopServer();
        }

        // Close Existing Client Connection
        if ( this.getNettyClient() != null ) {
            this.getNettyClient().disconnect();
        }

        this.nettyServer = new NettyServer();
        this.nettyServer.startServer( port, consumer );
    }

    public void registerPacketHandler( PacketHandler handler ) {
        if ( NettyHandler.getPacketHandlers().contains( handler ) ) {
            return;
        }

        NettyHandler.getPacketHandlers().add( handler );
    }

    public void unregisterPacketHandler( PacketHandler handler ) {
        if ( !NettyHandler.getPacketHandlers().contains( handler ) ) {
            return;
        }

        NettyHandler.getPacketHandlers().remove( handler );
    }

    public void unregisterAllPacketHandler() {
        NettyHandler.getPacketHandlers().clear();
    }

    public void registerConnectionListener( ConnectionListener handler ) {
        if ( NettyHandler.getConnectionListeners().contains( handler ) ) {
            return;
        }

        NettyHandler.getConnectionListeners().add( handler );
    }

    public void unregisterConnectionListener( ConnectionListener handler ) {
        if ( !NettyHandler.getConnectionListeners().contains( handler ) ) {
            return;
        }

        NettyHandler.getConnectionListeners().remove( handler );
    }

    public void unregisterAllConnectionListener() {
        NettyHandler.getConnectionListeners().clear();
    }

    public String getClientnameByChannel( Channel channel ) {
        for ( Map.Entry entry : NettyHandler.getClients().entrySet() ) {
            if ( entry.getValue() == channel ) {
                return (String) entry.getKey();
            }
        }

        return "";
    }

    public void addPacketCallback( Packet packet, Consumer<Packet> consumer ) {
        if ( !this.getPacketCallbacks().containsKey( packet.getUniqueId() ) ) {
            this.getPacketCallbacks().put( packet.getUniqueId(), new ArrayList<>() );
        }

        this.getPacketCallbacks().get( packet.getUniqueId() ).add( consumer );
    }

    public void removePacketCallbacks( Packet packet ) {
        if ( !this.getPacketCallbacks().containsKey( packet.getUniqueId() ) ) {
            return;
        }

        this.getPacketCallbacks().remove( packet.getUniqueId() );
    }

    public void runPacketCallbacks( Packet packet ) {
        if ( !this.getPacketCallbacks().containsKey( packet.getUniqueId() ) ) {
            return;
        }

        for ( Consumer<Packet> consumer : this.getPacketCallbacks().get( packet.getUniqueId() ) ) {
            consumer.accept( packet );
        }
    }

    public Map<UUID, ArrayList<Consumer<Packet>>> getPacketCallbacks() {
        return this.packetCallbacks;
    }

    public PacketHandler getHandler() {
        return this.handler;
    }

    public Type getType() {
        return this.type;
    }

    public NettyClient getNettyClient() {
        return this.nettyClient;
    }

    public NettyServer getNettyServer() {
        return this.nettyServer;
    }

    public void setHandler(PacketHandler handler) {
        this.handler = handler;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public enum Type {
        SERVER,
        CLIENT
    }
}
