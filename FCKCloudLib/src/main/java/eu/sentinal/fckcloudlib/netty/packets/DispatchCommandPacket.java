package eu.sentinal.fckcloudlib.netty.packets;

import eu.sentinal.fckcloudlib.netty.packet.Packet;
import io.netty.buffer.ByteBuf;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

public class DispatchCommandPacket extends Packet {

    private String commandline = "";

    public DispatchCommandPacket( String commandline ) {
        this.commandline = commandline;
    }

    @Override
    public void read( ByteBuf byteBuf ) throws IOException {
        int length;
        byte[] bytes;

        length = byteBuf.readInt();
        bytes = new byte[length];
        for ( int i = 0; i < length; i++ ) {
            bytes[i] = byteBuf.readByte();
        }
        this.commandline = new String( bytes );
    }

    @Override
    public void write( ByteBuf byteBuf ) throws IOException {
        byte[] bytes;

        bytes = this.getCommandline().getBytes( StandardCharsets.UTF_8 );
        byteBuf.writeInt( bytes.length );
        byteBuf.writeBytes( bytes );
    }

    @Override
    public String toString() {
        return "DispatchCommandPacket{" +
                "commandline='" + commandline + '\'' +
                ", uniqueId=" + uniqueId +
                '}';
    }

    public String getCommandline() {
        return this.commandline;
    }
}
