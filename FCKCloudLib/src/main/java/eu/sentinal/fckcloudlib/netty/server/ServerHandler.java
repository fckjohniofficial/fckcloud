package eu.sentinal.fckcloudlib.netty.server;

import eu.sentinal.fckcloudlib.netty.ConnectionListener;
import eu.sentinal.fckcloudlib.netty.NettyHandler;
import eu.sentinal.fckcloudlib.netty.PacketHandler;
import eu.sentinal.fckcloudlib.netty.packet.Packet;
import eu.sentinal.fckcloudlib.netty.packet.defaults.DisconnectPacket;
import eu.sentinal.fckcloudlib.netty.packet.defaults.SetNamePacket;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;

import java.util.Map;

public class ServerHandler extends SimpleChannelInboundHandler<Packet> {

    private Channel channel = null;

    private NettyServer nettyServer;

    public ServerHandler( NettyServer nettyServer ) {
        this.nettyServer = nettyServer;
    }

    @Override
    public void exceptionCaught( ChannelHandlerContext ctx, Throwable cause ) throws Exception {
        if ( NettyHandler.DEBUGMODE ) {
            super.exceptionCaught( ctx, cause );
        }
    }

    protected void channelRead0( ChannelHandlerContext ctx, Packet packet ) throws Exception {
        for ( PacketHandler handler : NettyHandler.getPacketHandlers() ) {
            handler.incomingPacket( packet, this.getChannel() );
        }

        if ( packet instanceof DisconnectPacket) {
            this.getChannel().close();
        }

        if ( packet instanceof SetNamePacket) {
            SetNamePacket setNamePacket = (SetNamePacket) packet;

            // Remove old entry
            String oldname = "";
            for ( Map.Entry entry : NettyHandler.getClients().entrySet() ) {
                String name = (String) entry.getKey();
                Channel channel = (Channel) entry.getValue();

                if ( channel == ctx.channel() ) {
                    oldname = name;
                    break;
                }
            }

            // Remove and add with new name
            if ( !oldname.equalsIgnoreCase( "" ) ) {
                NettyHandler.getClients().remove( oldname );
                NettyHandler.getClients().put( setNamePacket.getName(), ctx.channel() );
            }
        }

        NettyHandler.getInstance().runPacketCallbacks( packet );
    }

    @Override
    public void channelActive( ChannelHandlerContext ctx ) throws Exception {
        this.channel = ctx.channel();

        for ( ConnectionListener handler : NettyHandler.getConnectionListeners() ) {
            handler.channelConnected( ctx );
        }

        NettyHandler.getClients().put( "Channel" + NettyHandler.getClients().size() + 1, ctx.channel() );
    }

    @Override
    public void channelInactive( ChannelHandlerContext ctx ) throws Exception {
        //Logger.log( "Client disconnected" );
        this.channel = null;

        for ( ConnectionListener handler : NettyHandler.getConnectionListeners() ) {
            handler.channelDisconnected( ctx );
        }

        if ( NettyHandler.getClients().containsValue( ctx.channel() ) ) {
            String name = "";
            for ( Map.Entry entry : NettyHandler.getClients().entrySet() ) {
                if ( entry.getValue() == ctx.channel() ) {
                    name = (String) entry.getKey();
                    break;
                }
            }
            if ( !name.equalsIgnoreCase( "" ) ) {
                NettyHandler.getClients().remove( name );
            }
        }
    }

    public Channel getChannel() {
        return this.channel;
    }

    public NettyServer getNettyServer() {
        return this.nettyServer;
    }
}
