package eu.sentinal.fckcloudlib.netty.packet;

import io.netty.buffer.ByteBuf;

import java.util.UUID;

public abstract class Packet {

    public UUID uniqueId = UUID.randomUUID();


    public abstract void read( ByteBuf byteBuf ) throws Exception;

    public abstract void write( ByteBuf byteBuf ) throws Exception;

    @Override
    public String toString() {
        return this.getClass().getSimpleName() + "{" +
                "uniqueId=" + uniqueId +
                '}';
    }

    public UUID getUniqueId() {
        return this.uniqueId;
    }

    public void setUniqueId(UUID uniqueId) {
        this.uniqueId = uniqueId;
    }
}
