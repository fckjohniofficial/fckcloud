package eu.sentinal.fckcloudlib.netty.client;

import eu.sentinal.fckcloudlib.netty.NettyHandler;
import eu.sentinal.fckcloudlib.netty.packet.PacketDecoder;
import eu.sentinal.fckcloudlib.netty.packet.PacketEncoder;
import io.netty.bootstrap.Bootstrap;
import io.netty.channel.*;
import io.netty.channel.epoll.Epoll;
import io.netty.channel.epoll.EpollEventLoopGroup;
import io.netty.channel.epoll.EpollSocketChannel;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;

import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.Consumer;
import java.util.logging.Logger;

public class NettyClient {

    public static final boolean EPOLL = Epoll.isAvailable();
    public static ExecutorService POOL = Executors.newCachedThreadPool();

    private EventLoopGroup eventLoopGroup;

    private Bootstrap bootstrap;

    private ChannelFuture future;

    private String host = "localhost";

    private int port = 8000;

    private Channel channel;

    private Consumer<Boolean> consumer;

    public void connect( String host, int port, Consumer<Boolean> consumer ) {
        this.setHost( host );
        this.setPort( port );
        this.setConsumer( consumer );

        NettyClient.POOL.execute( () -> {
            this.eventLoopGroup = EPOLL ? new EpollEventLoopGroup() : new NioEventLoopGroup();
            try {
                this.bootstrap = new Bootstrap()
                        .group( this.getEventLoopGroup() )
                        .channel( EPOLL ? EpollSocketChannel.class : NioSocketChannel.class )
                        .option( ChannelOption.SO_BACKLOG, 128 )
                        .option( ChannelOption.SO_KEEPALIVE, true )
                        .handler( new ChannelInitializer<SocketChannel>() {
                            @Override
                            protected void initChannel( SocketChannel channel ) throws Exception {
                                channel.pipeline().addLast( new PacketEncoder() );
                                channel.pipeline().addLast( new PacketDecoder() );
                                channel.pipeline().addLast( new ClientHandler( NettyClient.this ) );
                            }
                        } );
                this.future = this.getBootstrap().connect( this.getHost(), this.getPort() );

                this.getFuture().sync();
                consumer.accept( true );

                this.getFuture().sync().channel().closeFuture().syncUninterruptibly();
            } catch ( Exception e ) {
                if ( NettyHandler.DEBUGMODE ) {
                    Logger.getGlobal().severe( e.getMessage());
                }

                this.setChannel( null );
                consumer.accept( false );
            } finally {
                if ( this.getEventLoopGroup() != null ) {
                    this.getEventLoopGroup().shutdownGracefully();
                }
                this.setChannel( null );
            }
        } );
    }

    public void scheduleConnect( int time ) {
        if ( this.isConnected() ) {
            return;
        }

        new Timer().schedule(
                new TimerTask() {
                    @Override
                    public void run() {
                        NettyClient.this.connect( getHost(), getPort(), NettyClient.this.getConsumer() );
                    }
                },
                time
        );
    }

    public void disconnect() {
        try {
            if ( this.getFuture() == null || this.getChannel() == null ) {
                return;
            }

            if ( !this.getChannel().isActive() ) {
                return;
            }

            this.getChannel().close();

            this.setChannel( null );
            this.future = null;
            this.bootstrap = null;
            this.eventLoopGroup = null;
        } catch ( Exception ignored ) {
        }
    }

    public void reconnect() {
        this.scheduleConnect( 0 );
    }

    public boolean isConnected() {
        return this.getChannel() != null && this.getChannel().isActive();
    }

    public EventLoopGroup getEventLoopGroup() {
        return this.eventLoopGroup;
    }

    public Bootstrap getBootstrap() {
        return this.bootstrap;
    }

    public ChannelFuture getFuture() {
        return this.future;
    }

    public String getHost() {
        return this.host;
    }

    public int getPort() {
        return this.port;
    }

    public Channel getChannel() {
        return this.channel;
    }

    public Consumer<Boolean> getConsumer() {
        return this.consumer;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public void setChannel(Channel channel) {
        this.channel = channel;
    }

    public void setConsumer(Consumer<Boolean> consumer) {
        this.consumer = consumer;
    }
}
