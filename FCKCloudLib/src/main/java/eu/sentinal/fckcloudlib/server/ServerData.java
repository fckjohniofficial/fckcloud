package eu.sentinal.fckcloudlib.server;

import io.netty.buffer.ByteBuf;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.charset.StandardCharsets;
import java.util.UUID;

public class ServerData {

    // BungeeCord Port Range
    public static final int PROXY_PORT_START = 25565;
    public static final int PROXY_PORT_END = 30000;

    // Spigot Port Range
    public static final int SERVER_PORT_START = 30000;
    public static final int SERVER_PORT_END = 70000;


    private int id = 0;

    private UUID uniqueId = UUID.randomUUID();

    private String host = "localhost";

    private int port = 0;

    private String name = "";

    private String motd = "";

    private int slots = 0;

    private int onlinePlayers = 0;


    public ServerData() {

        try {
            this.host = InetAddress.getLocalHost().getHostAddress();
        } catch ( UnknownHostException e ) {
            e.printStackTrace();
        }
    }

    public void toByteBuf( ByteBuf byteBuf ) {
        byte[] bytes;


        // ID
        byteBuf.writeInt( this.getId() );

        // UUID
        bytes = this.getUniqueId().toString().getBytes( StandardCharsets.UTF_8 );
        byteBuf.writeInt( bytes.length );
        byteBuf.writeBytes( bytes );

        // Host
        bytes = this.getHost().getBytes( StandardCharsets.UTF_8 );
        byteBuf.writeInt( bytes.length );
        byteBuf.writeBytes( bytes );

        // Port
        byteBuf.writeInt( this.getPort() );

        // Name
        bytes = this.getName().getBytes( StandardCharsets.UTF_8 );
        byteBuf.writeInt( bytes.length );
        byteBuf.writeBytes( bytes );

        // MOTD
        bytes = this.getMotd().getBytes( StandardCharsets.UTF_8 );
        byteBuf.writeInt( bytes.length );
        byteBuf.writeBytes( bytes );

        // Slots
        byteBuf.writeInt( this.getSlots() );

        // OnlinePlayers
        byteBuf.writeInt( this.getOnlinePlayers() );

    }

    public void fromByteBuf( ByteBuf byteBuf ) {
        int length;
        byte[] bytes;


        // ID
        this.setId( byteBuf.readInt() );

        // UUID
        length = byteBuf.readInt();
        bytes = new byte[length];
        for ( int i = 0; i < length; i++ ) {
            bytes[i] = byteBuf.readByte();
        }
        this.setUniqueId( UUID.fromString( new String( bytes ) ) );

        // Host
        length = byteBuf.readInt();
        bytes = new byte[length];
        for ( int i = 0; i < length; i++ ) {
            bytes[i] = byteBuf.readByte();
        }
        this.setHost( new String( bytes ) );

        // Port
        this.setPort( byteBuf.readInt() );

        // Name
        length = byteBuf.readInt();
        bytes = new byte[length];
        for ( int i = 0; i < length; i++ ) {
            bytes[i] = byteBuf.readByte();
        }
        this.setName( new String( bytes ) );

        // MOTD
        length = byteBuf.readInt();
        bytes = new byte[length];
        for ( int i = 0; i < length; i++ ) {
            bytes[i] = byteBuf.readByte();
        }
        this.setMotd( new String( bytes ) );

        // Slots
        this.setSlots( byteBuf.readInt() );

        // OnlinePlayers
        this.setOnlinePlayers( byteBuf.readInt() );

    }

    @Override
    public String toString() {
        return "ServerData{" +
                "id=" + id +
                ", uniqueId=" + uniqueId +
                ", port=" + port +
                ", name='" + name + '\'' +
                ", slots=" + slots +
                ", onlinePlayers=" + onlinePlayers +
                '}';
    }

    public int getId() {
        return this.id;
    }

    public UUID getUniqueId() {
        return this.uniqueId;
    }

    public String getHost() {
        return this.host;
    }

    public int getPort() {
        return this.port;
    }

    public String getName() {
        return this.name;
    }

    public String getMotd() {
        return this.motd;
    }

    public int getSlots() {
        return this.slots;
    }

    public int getOnlinePlayers() {
        return this.onlinePlayers;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setUniqueId(UUID uniqueId) {
        this.uniqueId = uniqueId;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setMotd(String motd) {
        this.motd = motd;
    }

    public void setSlots(int slots) {
        this.slots = slots;
    }

    public void setOnlinePlayers(int onlinePlayers) {
        this.onlinePlayers = onlinePlayers;
    }
}
