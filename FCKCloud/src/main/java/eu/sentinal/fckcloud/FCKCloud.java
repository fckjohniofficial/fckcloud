package eu.sentinal.fckcloud;

import eu.sentinal.fckcloud.commands.CommandHandler;
import eu.sentinal.fckcloud.commands.HelpCommand;
import eu.sentinal.fckcloud.commands.StopCommand;
import eu.sentinal.fckcloud.logging.CloudLogger;
import eu.sentinal.fckcloud.network.NetworkServer;
import eu.sentinal.fckcloud.server.BungeecordManager;
import eu.sentinal.fckcloud.server.ServerManager;
import eu.sentinal.fckcloud.server.SpigotManager;
import eu.sentinal.fckcloud.server.pojo.BungeeCordProxy;
import eu.sentinal.fckcloud.server.pojo.SpigotServer;
import eu.sentinal.fckcloud.server.pojo.server.ServerData;
import eu.sentinal.fckcloud.utils.FileManager;
import org.fusesource.jansi.AnsiConsole;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.UnknownHostException;
import java.nio.channels.Channels;
import java.nio.channels.FileChannel;
import java.nio.channels.ReadableByteChannel;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Logger;

public class FCKCloud {

    private List<BungeeCordProxy> bungeeCordProxies = new ArrayList<>();

    private List<SpigotServer> spigotServers = new ArrayList<>();

    private ExecutorService pool = Executors.newCachedThreadPool();

    private CommandHandler commandHandler;

    private BungeecordManager bungeecordManager;

    private SpigotManager spigotManager;

    private FileManager fileManager;

    private Logger logger;

    private NetworkServer network;

    private static FCKCloud instance;

    private ServerManager serverManager;

    public FCKCloud() {
        instance = this;

        AnsiConsole.systemInstall();

        this.logger = new CloudLogger();


        this.getLogger().info("§4  ____ _                 _ ____            _");
        this.getLogger().info("§4 / ___| | ___  _   _  __| / ___| _   _ ___| |_ ___ _ __ ___");
        this.getLogger().info("§4| |   | |/ _ \\| | | |/ _` \\___ \\| | | / __| __/ _ \\ '_ ` _ \\");
        this.getLogger().info("§4| |___| | (_) | |_| | (_| |___) | |_| \\__ \\ ||  __/ | | | | |");
        this.getLogger().info("§4 \\____|_|\\___/ \\__,_|\\__,_|____/ \\__, |___/\\__\\___|_| |_| |_|");
        this.getLogger().info("§6Developed by FCKJohni            §4|___/");

        this.getLogger().info("§cPlease wait, Generating file Structure!");

        fileManager = new FileManager();

        this.getPool().execute(new Thread(() -> {
            Thread.currentThread().setName("FCKCloudFile-Thread");
            Path path = Paths.get(getRootDirectory().getAbsolutePath(), "local", "temp");
            Path path_2 = Paths.get(getRootDirectory().getAbsolutePath(), "local", "plugins", "spigot");
            Path path_3 = Paths.get(getRootDirectory().getAbsolutePath(), "local", "plugins", "bungee");
            Path path_4 = Paths.get(getRootDirectory().getAbsolutePath(), "local", "templates", "proxy");
            Path path_5 = Paths.get(getRootDirectory().getAbsolutePath(), "local", "templates", "spigot");
            Path path_6 = Paths.get(getRootDirectory().getAbsolutePath(), "local", "logs");
            if (!new File(getRootDirectory().getAbsolutePath() + File.separator + "local" + File.separator + "templates" + File.separator + "proxy").exists()) {
                try {
                    Files.createDirectories(path);
                    Files.createDirectories(path_2);
                    Files.createDirectories(path_3);
                    Files.createDirectories(path_4);
                    Files.createDirectories(path_5);
                    Files.createDirectories(path_6);
                } catch (IOException e) {
                    e.printStackTrace();
                }

                try {
                    URL bungee = new URL("https://ci.md-5.net/job/BungeeCord/lastSuccessfulBuild/artifact/bootstrap/target/BungeeCord.jar");
                    Logger.getGlobal().info("Downloading newest Bungeecord.jar");
                    ReadableByteChannel readableByteChannel = Channels.newChannel(bungee.openStream());
                    FileOutputStream fileOutputStream = new FileOutputStream(path_4.toFile().getAbsoluteFile() + File.separator + "BungeeCord.jar");
                    FileChannel fileChannel = fileOutputStream.getChannel();

                    fileOutputStream.getChannel().transferFrom(readableByteChannel, 0, Long.MAX_VALUE);

                    URL spigot = new URL("https://cdn.getbukkit.org/spigot/spigot-1.16.2.jar");
                    Logger.getGlobal().info("Downloading newest Spigot-1.16.2.jar");
                    ReadableByteChannel readableChannel = Channels.newChannel(spigot.openStream());
                    FileOutputStream fileStream = new FileOutputStream(path_5.toFile().getAbsoluteFile() + File.separator + "Spigot-1.16.2.jar");
                    FileChannel channel = fileStream.getChannel();

                    fileStream.getChannel().transferFrom(readableChannel, 0, Long.MAX_VALUE);

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            getFileManager().copyProperties("1.16.2", new File(path_5.toFile().getAbsolutePath() + File.separator + "server.properties"));
            getFileManager().copySpigot("1.16.2", new File(path_5.toFile().getAbsolutePath() + File.separator + "spigot.yml"));


        }));


        Thread.currentThread().setName("FCKCloudMain-Thread");

        commandHandler = new CommandHandler();

        serverManager = new ServerManager();

        bungeecordManager = new BungeecordManager();

        spigotManager = new SpigotManager();

        try {
            network = new NetworkServer(this, 8000, Collections.singletonList(InetAddress.getLocalHost().getHostAddress()));
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }

        this.registerCommands();

        this.getPool().execute(() -> {
            Thread.currentThread().setName("FCKCloudCommands-Thread");
            this.getCommandHandler().consoleInput(System.out::println);
        });

        Runtime.getRuntime().addShutdownHook(new Thread(() -> {

            getServerManager().shutdown();
        }));

        ServerData serverData = new ServerData();
        serverData.setPort(getServerManager().getAvailableProxyPort());
        serverData.setUuid(UUID.randomUUID());
        serverData.setMotd("Lol");
        serverData.setName("Proxy-1");
        new BungeeCordProxy(serverData).startProxy();

        ServerData spigot = new ServerData();
        spigot.setPort(getServerManager().getAvailablePort());
        spigot.setUuid(UUID.randomUUID());
        spigot.setMotd("Lol");
        spigot.setName("Spigot-1");
        new SpigotServer(spigot).startSpigot();

    }

    public static File getRootDirectory() {
        File directory = null;

        try {
            directory = new File(FCKCloud.class.getProtectionDomain().getCodeSource().getLocation().toURI().getPath()).getParentFile();
            if (!directory.isDirectory()) {
                directory.mkdir();
            }
        } catch (URISyntaxException e) {
            Logger.getGlobal().severe(e.getMessage());
        }

        return directory;
    }

    public static FCKCloud getInstance() {
        return FCKCloud.instance;
    }

    public File getRoot() {
        return getRootDirectory();
    }

    public File getTemplate() {
        return new File(getRoot().getAbsolutePath() + File.separator + "local" + File.separator + "templates");
    }

    /*public static FCKCloud getInstance(){
        return instance;
    }
    */

    private void registerCommands() {
        this.getCommandHandler().registerCommand(new HelpCommand());
        this.getCommandHandler().registerCommand(new StopCommand());
    }

    public static void main(String[] args) {
        new FCKCloud();
    }

    public synchronized Process startProcess(ProcessBuilder processBuilder) {
        try {
            return processBuilder.start();
        } catch (IOException e) {
            Logger.getGlobal().severe(e.getMessage());
        }

        return null;
    }

    public void shutdown() {
        for (Iterator<BungeeCordProxy> itr = getBungeeCordProxies().iterator(); itr.hasNext(); ) {
            BungeeCordProxy bungeeCordProxy = itr.next();
            bungeeCordProxy.shutdown();
            itr.remove();
        }

        for (Iterator<SpigotServer> iterator = getSpigotServers().iterator(); iterator.hasNext(); ) {
            SpigotServer spigotServer = iterator.next();
            spigotServer.shutdown();
            iterator.remove();
        }
        this.getPool().shutdown();
        System.exit(0);
    }

    public List<BungeeCordProxy> getBungeeCordProxies() {
        return this.bungeeCordProxies;
    }

    public List<SpigotServer> getSpigotServers() {
        return this.spigotServers;
    }

    public ExecutorService getPool() {
        return this.pool;
    }

    public CommandHandler getCommandHandler() {
        return this.commandHandler;
    }

    public BungeecordManager getBungeecordManager() {
        return this.bungeecordManager;
    }

    public SpigotManager getSpigotManager() {
        return this.spigotManager;
    }

    public FileManager getFileManager() {
        return this.fileManager;
    }

    public Logger getLogger() {
        return this.logger;
    }

    public NetworkServer getNetwork() {
        return this.network;
    }

    public ServerManager getServerManager() {
        return this.serverManager;
    }
}
