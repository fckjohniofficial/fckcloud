package eu.sentinal.fckcloud.commands;

import eu.sentinal.fckcloud.FCKCloud;

public class StopCommand extends Command{

    public StopCommand(){
        super("stop");
        this.getAliases().add("disconnect");
        this.getAliases().add("close");
        this.getAliases().add("end");
    }

    @Override
    public void execute(String label, String[] args) {
        FCKCloud.getInstance().shutdown();
    }
}
