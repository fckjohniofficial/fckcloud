package eu.sentinal.fckcloud.commands;

import java.util.ArrayList;

public abstract class Command {

    private String name = "";

    private ArrayList<String> aliases = new ArrayList<>();

    public Command( String name ) {
        this.name = name;
    }

    public Command( String name, ArrayList<String> aliases ) {
        this.name = name;
        this.aliases = aliases;
    }

    public abstract void execute( String label, String[] args );

    public String getName() {
        return this.name;
    }

    public ArrayList<String> getAliases() {
        return this.aliases;
    }
}
