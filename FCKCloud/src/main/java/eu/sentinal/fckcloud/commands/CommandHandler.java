package eu.sentinal.fckcloud.commands;

import eu.sentinal.fckcloud.logging.CloudLogger;

import java.io.IOException;
import java.util.ArrayList;
import java.util.function.Consumer;

public class CommandHandler {

    public ArrayList<Command> commands = new ArrayList<>();

    public void registerCommand( Command command ) {
        for ( Command cmd : getCommands() ) {
            if ( command.getName().equalsIgnoreCase( cmd.getName() ) ) {
                CloudLogger.getGlobal().warning( "The name of the command '" + command.getName() + "' is already in use." );
            }

            if ( cmd.getAliases().contains( command.getName() ) ) {
                CloudLogger.getGlobal().warning( "An alias is already registered with the name " + command.getName() + "." );
            }

            for ( String alias : command.getAliases() ) {
                if ( cmd.getAliases().contains( alias ) ) {
                    CloudLogger.getGlobal().warning( "The alias '" + alias + "' is already an alias of an other command." );
                    break;
                }
            }
        }

        getCommands().add( command );
    }

    public String onExecute( String message ) {
        String commandname = message.split( " " )[0];
        Command command = getCommandByName( commandname );

        if ( command == null ) {
            return commandname;
        }

        ArrayList<String> args = new ArrayList<>();
        for ( String argument : message.substring( commandname.length() ).split( " " ) ) {
            if ( argument.equalsIgnoreCase( "" ) || argument.equalsIgnoreCase( " " ) ) {
                continue;
            }

            args.add( argument );
        }

        command.execute( commandname, args.toArray( new String[args.size()] ) );
        return null;
    }

    public Command getCommandByName( String name ) {
        for ( Command command : getCommands() ) {
            if ( command.getName().equalsIgnoreCase( name ) ) {
                return command;
            }

            for ( String alias : command.getAliases() ) {
                if ( alias.equalsIgnoreCase( name ) ) {
                    return command;
                }
            }
        }

        return null;
    }

    public boolean commandExist( Command command ) {
        return getCommandByName( command.getName() ) != null;
    }

    public void consoleInput( Consumer<String> consumer ) {
        try {
            String input;
            while ( ( input = CloudLogger.getReader().readLine() ) != null ) {
                if ( !input.equalsIgnoreCase( "" ) ) {
                    String commandname = this.onExecute( input );
                    if ( commandname != null ) {
                        consumer.accept( this.onExecute( commandname ) );
                    }
                }
            }
        } catch ( IOException e ) {
            CloudLogger.getGlobal().severe( e.getMessage());
        }

        this.consoleInput( consumer );
    }

    public ArrayList<Command> getCommands() {
        return this.commands;
    }
}
