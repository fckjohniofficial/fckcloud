package eu.sentinal.fckcloud.commands;

import eu.sentinal.fckcloud.FCKCloud;

public class HelpCommand extends Command{

    public HelpCommand(){
        super("help");

        getAliases().add("?");
        getAliases().add("commands");
    }

    @Override
    public void execute(String label, String[] args) {
        for ( Command command : FCKCloud.getInstance().getCommandHandler().getCommands() ) {
            FCKCloud.getInstance().getLogger().info( "- " + command.getName() + " (Aliases: " + command.getAliases().toString().replace( "[", "" ).replace( "]", "" ) + ")" );
        }
    }
}
