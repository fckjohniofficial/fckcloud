package eu.sentinal.fckcloud.server;

import eu.sentinal.fckcloud.FCKCloud;

import java.util.ArrayList;

public class BungeecordManager {

    FCKCloud fckCloud;

    public BungeecordManager(){
        fckCloud = FCKCloud.getInstance();
    }

    public void notifyStartup(String UUID){
        fckCloud.getLogger().info("A Proxy with UUID [" + UUID + "] is starting!");
    }

    public void notifyShutdown(String UUID){
        fckCloud.getLogger().info("A Proxy with UUID [" + UUID + "] is Shutting down!");
    }

    public void notifyForceShutdown(String UUID){
        fckCloud.getLogger().info("Proxy with UUID [" + UUID + "] could not be shut down gracefully, killing Process!");
    }

    public void templateMissing(String UUID){
        fckCloud.getLogger().severe("Proxy with UUID [" + UUID + "] failed to start, the Template is missing!");
    }

}
