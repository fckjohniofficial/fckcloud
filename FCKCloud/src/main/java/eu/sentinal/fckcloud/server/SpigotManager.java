package eu.sentinal.fckcloud.server;

import eu.sentinal.fckcloud.FCKCloud;

public class SpigotManager {

    FCKCloud fckCloud;

    public SpigotManager(){
        fckCloud = FCKCloud.getInstance();
    }

    public void notifyStartup(String UUID){
        fckCloud.getLogger().info("A Server with UUID [" + UUID + "] is starting!");
    }

    public void notifyShutdown(String UUID){
        fckCloud.getLogger().info("A Server with UUID [" + UUID + "] is Shutting down!");
    }

    public void notifyForceShutdown(String UUID){
        fckCloud.getLogger().info("Server with UUID [" + UUID + "] could not be shut down gracefully, killing Process!");
    }

    public void templateMissing(String UUID){
        fckCloud.getLogger().severe("Server with UUID [" + UUID + "] failed to start, the Template is missing!");
    }
}
