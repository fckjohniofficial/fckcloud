package eu.sentinal.fckcloud.server;

import eu.sentinal.fckcloud.FCKCloud;
import eu.sentinal.fckcloud.server.pojo.BungeeCordProxy;
import eu.sentinal.fckcloud.server.pojo.SpigotServer;

import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

public class ServerManager {

    public static final int PROXY_PORT_RANGE_START = 25565;
    public static final int PROXY_PORT_RANGE_END = 30000;
    public List<Integer> PROXY_PORTS_IN_USE = new ArrayList<>();

    public static final int SERVER_PORT_RANGE_START = 30000;
    public static final int SERVER_PORT_RANGE_END = 70000;
    public List<Integer> SERVER_PORTS_IN_USE = new ArrayList<>();

    public void shutdown() {
        this.PROXY_PORTS_IN_USE.clear();
        this.SERVER_PORTS_IN_USE.clear();
    }

    public int getAvailableProxyPort() {
        for (int port = PROXY_PORT_RANGE_START; port < PROXY_PORT_RANGE_END; port++) {
            if (PROXY_PORTS_IN_USE.contains(port)) {
                continue;
            }
            try {
                if (!isPortInUse(InetAddress.getLocalHost().getHostAddress(), port)) {
                    return port;
                }
            } catch (UnknownHostException e) {
                e.printStackTrace();
            }
        }
        return 0;
    }

    public int getAvailablePort() {
        for (int port = SERVER_PORT_RANGE_START; port < SERVER_PORT_RANGE_END; port++) {
            if (SERVER_PORTS_IN_USE.contains(port)) {
                continue;
            }
            try {
                if (!isPortInUse(InetAddress.getLocalHost().getHostAddress(), port)) {
                    return port;
                }
            } catch (UnknownHostException e) {
                e.printStackTrace();
            }
        }
        return 0;
    }

    private boolean isPortInUse(String host, int port) {
        boolean result = false;

        try {
            (new Socket(host, port)).close();
            result = true;
        } catch (SocketException ignored) {
        } catch (IOException e) {
            e.printStackTrace();
        }

        return result;
    }

    public SpigotServer getServerByName(String name) {
        for (SpigotServer spigotServer : FCKCloud.getInstance().getSpigotServers()) {
            if(spigotServer.getName().equalsIgnoreCase(name)){
                return spigotServer;
            }
        }
        return null;
    }

    public BungeeCordProxy getProxyByName(String name) {
        for (BungeeCordProxy bungeeCordProxy : FCKCloud.getInstance().getBungeeCordProxies()) {
            if(bungeeCordProxy.getName().equalsIgnoreCase(name)){
                return bungeeCordProxy;
            }
        }
        return null;
    }
}
