package eu.sentinal.fckcloud.server.pojo;

import eu.sentinal.fckcloud.FCKCloud;
import eu.sentinal.fckcloud.server.pojo.server.ServerData;
import eu.sentinal.fckcloudlib.config.Config;
import eu.sentinal.fckcloudlib.netty.packet.defaults.SetNamePacket;
import eu.sentinal.fckcloudlib.netty.packets.UnregisterServerPacket;
import org.apache.commons.io.FileUtils;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.InetAddress;
import java.nio.charset.StandardCharsets;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

public class SpigotServer extends ServerData {

    private final FCKCloud fckCloud;

    private Process process;

    private BufferedWriter bufferedWriter;

    public SpigotServer(ServerData serverData) {
        this.fckCloud = FCKCloud.getInstance();
        setPort(serverData.getPort());
        setMotd(serverData.getMotd());
        setName(serverData.getName());
        setUuid(serverData.getUuid());
    }

    public void startSpigot() {
        fckCloud.getSpigotManager().notifyStartup(getUuid().toString());

        File serverDirectory = this.initServerDirectory();
        this.startProcess(serverDirectory);
        /*eu.sentinal.fckcloudlib.server.ServerData serverData = new eu.sentinal.fckcloudlib.server.ServerData();
        serverData.setHost(getInet());
        serverData.setPort(getPort());
        serverData.setUniqueId(getUuid());
        serverData.setMotd(getMotd());
        serverData.setName(getName());
        RegisterServerPacket registerServerPacket = new RegisterServerPacket(serverData);
        fckCloud.getNetwork().getPacketHandler().sendPacket(registerServerPacket, fckCloud.getNetwork().getNettyHandler().getClients().get("Proxy-1"));
        */
        SetNamePacket setNamePacket = new SetNamePacket();
        setNamePacket.setUniqueId(getUuid());
        setNamePacket.setName("Lobby-1");
        fckCloud.getNetwork().getPacketHandler().sendPacket(setNamePacket);

        this.getFckCloud().getSpigotServers().add(this);
        this.getFckCloud().getServerManager().SERVER_PORTS_IN_USE.add(getPort());
    }

    private File initServerDirectory() {
        File templateFolder = new File(fckCloud.getTemplate().getAbsolutePath(), "spigot");
        if (!templateFolder.exists()) {
            fckCloud.getSpigotManager().templateMissing(getUuid().toString());
            return null;
        }

        File serverDirectory = new File(FCKCloud.getRootDirectory(), "local/temp/" + this.getUuid());
        if (!serverDirectory.exists()) {
            serverDirectory.mkdirs();
        } else {
            Path path = Paths.get(serverDirectory.toURI());
            try {
                Files.walkFileTree(path, new SimpleFileVisitor<Path>() {

                    @Override
                    public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
                        Files.delete(dir);
                        return FileVisitResult.CONTINUE;
                    }

                    @Override
                    public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                        Files.delete(file);
                        return FileVisitResult.CONTINUE;
                    }


                });
            } catch (IOException e) {
                System.out.println("cause1");
                Logger.getGlobal().severe(e.getMessage());
            }
        }

        try {
            FileUtils.copyDirectory(templateFolder, serverDirectory);
        } catch (IOException e) {
            System.out.println("cause2");
            Logger.getGlobal().severe(e.getMessage());
        }

        // Copy global bukkit plugins (and also CloudAPI)
        try {
            FileUtils.copyDirectory(new File(FCKCloud.getRootDirectory(), "local/plugins/spigot"), new File(serverDirectory, "plugins"));

            File directory = new File(serverDirectory, "plugins/CloudAPI");
            if (!directory.exists()) {
                directory.mkdirs();
            }
            Config dataConfig = new Config(new File(directory, "data.yml"), Config.YAML);
            dataConfig.set("servername", this.getName());
            dataConfig.set("uuid", this.getUuid().toString());
            dataConfig.set("address", getInet());
            dataConfig.set("port", getPort());
            dataConfig.save();
        } catch (IOException | NullPointerException e) {
            System.out.println("cause3 ");
            e.printStackTrace();
            Logger.getGlobal().severe(e.getMessage());
        }

        File properties = new File(serverDirectory, "server.properties");
        try {
            String lines = new String(Files.readAllBytes(properties.toPath()), StandardCharsets.UTF_8);
            String[] splitLines = lines.split("\n");

            int i = 0;
            for (String line : splitLines) {
                switch (line.split("=")[0]) {
                    case "server-port":
                        lines = lines.replace(line, "server-port=" + this.getPort());
                        break;
                    case "online-mode":
                        lines = lines.replace(line, "online-mode=false");
                        break;
                    case "server-ip":
                        lines = lines.replace(line, "server-ip=" + InetAddress.getLocalHost().getHostAddress());
                        break;
                }
                i++;
            }

            Files.write(properties.toPath(), lines.getBytes());
        } catch (IOException ex) {
            System.out.println("cause4");
            Logger.getGlobal().severe(ex.getMessage());
        }

        properties = new File(serverDirectory, "spigot.yml");
        try {
            String lines = new String(Files.readAllBytes(properties.toPath()), StandardCharsets.UTF_8);
            String[] splitLines = lines.split("\n");

            for (String line : splitLines) {
                switch (line.split(":")[0]) {
                    case "bungeecord":
                        lines = lines.replace(line, line.replace("false", "true"));
                        break;
                    case "restart-on-crash":
                        lines = lines.replace(line, line.replace("true", "false"));
                        break;
                }
            }

            Files.write(properties.toPath(), lines.getBytes());
        } catch (IOException ex) {
            System.out.println("cause5");
            Logger.getGlobal().severe(ex.getMessage());
        }

        return serverDirectory;
    }

    private void startProcess(File serverDirectory) {
        String spigotJarName = "spigot.jar";
        if (serverDirectory != null && serverDirectory.exists()) {
            for (File file : serverDirectory.listFiles()) {
                if (file.getName().toLowerCase().endsWith(".jar")) {
                    if (file.getName().toLowerCase().contains("spigot") || file.getName().toLowerCase().contains("server")) {
                        spigotJarName = file.getName();
                        break;
                    }
                }
            }
        }

        ProcessBuilder processBuilder = new ProcessBuilder();
        processBuilder.directory(serverDirectory);
        processBuilder.command("java", "-Djline.terminal=jline.UnsupportedTerminal", "-Dcom.mojang.eula.agree=true", "-jar", spigotJarName, "nogui");

        FCKCloud.getInstance().getPool().execute(() -> {
            try {
                this.process = FCKCloud.getInstance().startProcess(processBuilder);
                this.bufferedWriter = new BufferedWriter(new OutputStreamWriter(this.getProcess().getOutputStream()));

                int exitCode = this.process.waitFor();
                this.forceShutdown();
            } catch (InterruptedException e) {
                Logger.getGlobal().severe(e.getMessage());
            }
        });
    }

    public void forceShutdown() {

        if (this.getProcess() != null) {
            if (this.getProcess().isAlive()) {
                fckCloud.getSpigotManager().notifyForceShutdown(getUuid().toString());
                this.getProcess().destroy();
                try {
                    this.getBufferedWriter().close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        deleteFolder();


    }

    public void shutdown() {
        fckCloud.getSpigotManager().notifyShutdown(getUuid().toString());
        eu.sentinal.fckcloudlib.server.ServerData serverData = new eu.sentinal.fckcloudlib.server.ServerData();
        serverData.setHost(getInet());
        serverData.setPort(getPort());
        serverData.setUniqueId(getUuid());
        serverData.setMotd(getMotd());
        serverData.setName(getName());
        UnregisterServerPacket unregisterServerPacket = new UnregisterServerPacket(serverData);
        fckCloud.getNetwork().getPacketHandler().sendPacket(unregisterServerPacket);
        execute("stop");
        try {
            boolean val = this.getProcess().waitFor(5, TimeUnit.SECONDS);
            if (!val) {
                forceShutdown();
                return;
            }
            deleteFolder();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void deleteFolder() {
        try {
            Path path = Paths.get(FCKCloud.getRootDirectory().getAbsolutePath(), "local", "temp", this.getUuid().toString());
            Files.walkFileTree(path, new SimpleFileVisitor<Path>() {
                @Override
                public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                    Files.delete(file);
                    return FileVisitResult.CONTINUE;
                }

                @Override
                public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
                    Files.delete(dir);
                    return FileVisitResult.CONTINUE;
                }
            });
        } catch (IOException e) {
            //Logger.getGlobal().severe(e.getMessage());
        }
    }

    public void execute(String commandline) {
        if (this.getProcess() == null) {
            return;
        }

        if (!this.getProcess().isAlive()) {
            return;
        }

        FCKCloud.getInstance().getPool().execute(() -> {
            try {
                if (this.getBufferedWriter() == null) {
                    return;
                }

                this.getBufferedWriter().write(commandline);
                this.getBufferedWriter().newLine();
                this.getBufferedWriter().flush();
            } catch (IOException e) {
                Logger.getGlobal().severe(e.getMessage());
            }
        });
    }

    public FCKCloud getFckCloud() {
        return this.fckCloud;
    }

    public Process getProcess() {
        return this.process;
    }

    public BufferedWriter getBufferedWriter() {
        return this.bufferedWriter;
    }
}
