package eu.sentinal.fckcloud.server.pojo.server;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.UUID;

public class ServerData {

    private int port;

    private String motd;

    private String inet;

    private UUID uuid;

    private String name;

    public ServerData(){
        try {
            this.inet = InetAddress.getLocalHost().getHostAddress();
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
    }

    public int getPort() {
        return this.port;
    }

    public String getMotd() {
        return this.motd;
    }

    public String getInet() {
        return this.inet;
    }

    public UUID getUuid() {
        return this.uuid;
    }

    public String getName() {
        return this.name;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public void setMotd(String motd) {
        this.motd = motd;
    }

    public void setInet(String inet) {
        this.inet = inet;
    }

    public void setUuid(UUID uuid) {
        this.uuid = uuid;
    }

    public void setName(String name) {
        this.name = name;
    }
}
