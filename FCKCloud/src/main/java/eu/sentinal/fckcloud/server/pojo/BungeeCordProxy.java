package eu.sentinal.fckcloud.server.pojo;

import eu.sentinal.fckcloud.FCKCloud;
import eu.sentinal.fckcloud.server.pojo.server.ServerData;
import eu.sentinal.fckcloudlib.config.Config;
import eu.sentinal.fckcloudlib.netty.packet.defaults.SetNamePacket;
import eu.sentinal.fckcloudlib.netty.packets.UnregisterServerPacket;
import org.apache.commons.io.FileUtils;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.logging.Logger;

public class BungeeCordProxy extends ServerData {

    private FCKCloud fckCloud = FCKCloud.getInstance();

    private Process process;

    private BufferedWriter bufferedWriter;

    public BungeeCordProxy(ServerData serverData) {
        setPort(serverData.getPort());
        setMotd(serverData.getMotd());
        setName(serverData.getName());
        setUuid(serverData.getUuid());
    }

    public void startProxy() {
        fckCloud.getBungeecordManager().notifyStartup(getUuid().toString());

        File serverDirectory = this.initProxyDirectory();
        this.startProcess(serverDirectory);
        eu.sentinal.fckcloudlib.server.ServerData serverData = new eu.sentinal.fckcloudlib.server.ServerData();
        serverData.setUniqueId(getUuid());
        serverData.setPort(getPort());
        serverData.setHost(getInet());
        serverData.setMotd(getMotd());
        SetNamePacket setNamePacket = new SetNamePacket();
        setNamePacket.setUniqueId(getUuid());
        setNamePacket.setName("Proxy-1");
        fckCloud.getNetwork().getPacketHandler().sendPacket(setNamePacket);

        this.getFckCloud().getBungeeCordProxies().add(this);
        this.getFckCloud().getServerManager().PROXY_PORTS_IN_USE.add(getPort());
    }

    private File initProxyDirectory() {
        File template = new File(fckCloud.getTemplate().getAbsolutePath() + File.separator + "proxy");
        if (!template.exists()) {
            fckCloud.getBungeecordManager().templateMissing(getUuid().toString());
            return null;
        }

        File server = new File(fckCloud.getRoot(), "local" + File.separator + "temp" + File.separator + this.getUuid());
        if (!server.exists()) {
            server.mkdirs();
        } else {
            deleteFolder();
            server.mkdirs();
        }

        try {
            FileUtils.copyDirectory(template, server);
        } catch (IOException e) {
            Logger.getGlobal().severe(e.getMessage());
        }
        try {
            FileUtils.copyDirectory(new File(FCKCloud.getRootDirectory(), "local/plugins/bungee"), new File(server, "plugins"));

            File directory = new File( server, "plugins/CloudAPI" );
            if ( !directory.exists() ) {
                directory.mkdirs();
            }
            Config dataConfig = new Config( new File( directory, "data.yml" ), Config.YAML );
            dataConfig.set( "servername", this.getName() );
            dataConfig.set( "uuid", this.getUuid().toString() );
            dataConfig.set( "address", getInet() );
            dataConfig.set( "port", getPort() );
            dataConfig.save();
        } catch (IOException | NullPointerException e) {
            Logger.getGlobal().severe(e.getMessage());
        }
        return server;
    }

    public void shutdown() {
        fckCloud.getBungeecordManager().notifyShutdown(getUuid().toString());
        execute("end");
        try {
            int i = this.getProcess().waitFor();
            deleteFolder();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void deleteFolder() {
        try {
            Path path = Paths.get(FCKCloud.getRootDirectory().getAbsolutePath(), "local", "temp", this.getUuid().toString());
            Files.walkFileTree(path, new SimpleFileVisitor<Path>() {
                @Override
                public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                    Files.delete(file);
                    return FileVisitResult.CONTINUE;
                }

                @Override
                public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
                    Files.delete(dir);
                    return FileVisitResult.CONTINUE;
                }
            });
        } catch (IOException e) {
            //Logger.getGlobal().severe(e.getMessage());
        }
    }

    public void forceShutdown() {
        if (this.getProcess() != null) {
            if (this.getProcess().isAlive()) {
                fckCloud.getBungeecordManager().notifyForceShutdown(getUuid().toString());
                this.getProcess().destroy();
                try {
                    this.getBufferedWriter().close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        deleteFolder();
        eu.sentinal.fckcloudlib.server.ServerData serverData = new eu.sentinal.fckcloudlib.server.ServerData();
        serverData.setHost(getInet());
        serverData.setPort(getPort());
        serverData.setUniqueId(getUuid());
        UnregisterServerPacket unregisterServerPacket = new UnregisterServerPacket(serverData);
        fckCloud.getNetwork().getPacketHandler().sendPacket(unregisterServerPacket);

    }

    private void startProcess(File workingDirectory) {
        String bungeeCordJar = "BungeeCord.jar";
        if (workingDirectory != null && workingDirectory.exists()) {
            for (File file : workingDirectory.listFiles()) {
                if (file.getName().toLowerCase().endsWith(".jar")) {
                    if (file.getName().toLowerCase().contains("bungee") || file.getName().toLowerCase().contains("proxy")) {
                        bungeeCordJar = file.getName();
                        break;
                    }
                }
            }
        } else {
            return;
        }

        ProcessBuilder processBuilder = new ProcessBuilder();
        processBuilder.directory(workingDirectory);
        processBuilder.command("java", "-Djline.terminal=jline.UnsupportedTerminal", "-jar", bungeeCordJar);


        FCKCloud.getInstance().getPool().execute(() -> {
            try {
                this.process = fckCloud.startProcess(processBuilder);
                this.bufferedWriter = new BufferedWriter(new OutputStreamWriter(this.getProcess().getOutputStream()));

                this.process.waitFor();
                this.forceShutdown();
            } catch (InterruptedException e) {
                Logger.getGlobal().severe(e.getMessage());
            }
        });
    }

    public void execute(String commandLine) {
        if (this.getProcess() == null) {
            return;
        }

        if (!this.getProcess().isAlive()) {
            return;
        }


        FCKCloud.getInstance().getPool().execute(() -> {
            try {
                this.getBufferedWriter().write(commandLine);
                this.getBufferedWriter().newLine();
                this.getBufferedWriter().flush();
                System.out.println("3");
            } catch (IOException e) {
                Logger.getGlobal().severe(e.getMessage());
            }
        });
    }

    public FCKCloud getFckCloud() {
        return this.fckCloud;
    }

    public Process getProcess() {
        return this.process;
    }

    public BufferedWriter getBufferedWriter() {
        return this.bufferedWriter;
    }
}
