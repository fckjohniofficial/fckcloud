package eu.sentinal.fckcloud.logging;

import eu.sentinal.fckcloud.FCKCloud;
import eu.sentinal.fckcloud.logging.handler.ColoredConsoleHandler;
import eu.sentinal.fckcloud.logging.stream.LoggingOutputStream;
import jline.console.ConsoleReader;

import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;

public class CloudLogger extends Logger {

    private static ConsoleReader consoleReader;

    public CloudLogger() {
        super("Logger", null);
        setLevel(Level.ALL);
        Path logsFolder = Paths.get(FCKCloud.getRootDirectory().getAbsolutePath() + File.separator + "local", "logs");
        try {
            Files.createDirectories(logsFolder);
            FileHandler fileHandler = new FileHandler("local" + File.separator + "logs" + File.separator + "latest.%g.log", 80000, 10, true);
            fileHandler.setFormatter(new LoggingFormatter());
            fileHandler.setEncoding(StandardCharsets.UTF_8.name());
            fileHandler.setLevel(Level.ALL);
            consoleReader = new ConsoleReader(System.in, System.out);
            consoleReader.setExpandEvents(true);
            addHandler(fileHandler);
            ColoredConsoleHandler coloredConsoleHandler = new ColoredConsoleHandler();
            coloredConsoleHandler.setLevel(Level.INFO);
            coloredConsoleHandler.setFormatter(new LoggingFormatter());
            coloredConsoleHandler.setEncoding(StandardCharsets.UTF_8.name());
            addHandler(coloredConsoleHandler);
            System.setOut(new PrintStream(new LoggingOutputStream(this, Level.INFO), true, StandardCharsets.UTF_8.name()));
            System.setErr(new PrintStream(new LoggingOutputStream(this, Level.SEVERE), true, StandardCharsets.UTF_8.name()));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static ConsoleReader getReader() {
        return consoleReader;
    }


}
