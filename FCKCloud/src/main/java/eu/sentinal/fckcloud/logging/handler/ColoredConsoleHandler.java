package eu.sentinal.fckcloud.logging.handler;

import eu.sentinal.fckcloud.logging.color.AnsiColorReplacer;

import java.util.logging.ConsoleHandler;
import java.util.logging.LogRecord;

public class ColoredConsoleHandler extends ConsoleHandler {

    @Override
    public void publish(LogRecord record) {
        record.setMessage(AnsiColorReplacer.replaceAnsi(record.getMessage()));
        super.publish(record);
    }
}
