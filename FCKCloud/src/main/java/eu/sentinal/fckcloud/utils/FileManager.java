package eu.sentinal.fckcloud.utils;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;

public class FileManager {

    public void copyProperties(String version, File dest) {
        copyFile("classpath:server.properties-" + version, dest);
    }

    public void copySpigot(String version, File dest) {
        copyFile("classpath:spigot.yml-" + version, dest);
    }

    public void copyFile(String source, File dest) {
        InputStream inputStream = getClass().getClassLoader().getResourceAsStream(source);
        if (inputStream == null) return;
        try {
            Files.copy(inputStream, dest.toPath());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
