package eu.sentinal.fckcloud.network;

import eu.sentinal.fckcloud.FCKCloud;
import eu.sentinal.fckcloud.server.pojo.BungeeCordProxy;
import eu.sentinal.fckcloud.server.pojo.SpigotServer;
import eu.sentinal.fckcloudlib.netty.ConnectionListener;
import eu.sentinal.fckcloudlib.netty.NettyHandler;
import eu.sentinal.fckcloudlib.netty.PacketHandler;
import eu.sentinal.fckcloudlib.netty.packet.defaults.SetNamePacket;
import io.netty.channel.ChannelHandlerContext;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.util.function.Consumer;
import java.util.logging.Logger;

public class Network {

    private String host = "localhost";

    private int port = 19132;

    private FCKCloud cloudWrapper;

    private NettyHandler nettyHandler;

    private ConnectionListener connectionListener;

    private PacketHandler packetHandler;

    private Consumer<Boolean> connectingConsumer;

    public Network(FCKCloud cloudWrapper, String host, int port) {
        this.cloudWrapper = cloudWrapper;
        this.host = host;
        this.port = port;

        this.nettyHandler = new NettyHandler();
        this.getNettyHandler().connectToServer(this.getHost(), this.getPort(), this.connectingConsumer = new Consumer<Boolean>() {
            @Override
            public void accept(Boolean success) {
                if (success) {
                    Logger.getGlobal().info("Connection to API Successfull");

                    SetNamePacket setNamePacket = new SetNamePacket("Unnamed-Wrapper");
                    Network.this.getPacketHandler().sendPacket(setNamePacket);
                } else {
                    Logger.getGlobal().severe("Connection failed");
                    Logger.getGlobal().severe("Attempting reconnect");
                    nettyHandler.reconnectToServer(3, this);
                }
            }
        });

        this.getNettyHandler().registerConnectionListener(this.connectionListener = new ConnectionListener() {
            @Override
            public void channelConnected(ChannelHandlerContext ctx) {
            }

            @Override
            public void channelDisconnected(ChannelHandlerContext ctx) {
                for (BungeeCordProxy bungeeCordProxy : Network.this.getCloudWrapper().getBungeeCordProxies()) {
                    bungeeCordProxy.forceShutdown();
                }
                for (SpigotServer spigotServer : Network.this.getCloudWrapper().getSpigotServers()) {
                    spigotServer.forceShutdown();
                }
                Logger.getGlobal().severe("Lost connection to API");
                Logger.getGlobal().severe("Attempting reconnect");
                nettyHandler.reconnectToServer(3, Network.this.getConnectingConsumer());

            }
        });
    }


    private void copyDirectoryRecursive(File src, File dest) {
        try {
            for (File file : src.listFiles()) {
                if (file.isDirectory()) {
                    this.copyDirectoryRecursive(file, new File(dest, file.getName()));
                } else {
                    FileUtils.copyFileToDirectory(file, dest);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String getHost() {
        return this.host;
    }

    public int getPort() {
        return this.port;
    }

    public FCKCloud getCloudWrapper() {
        return this.cloudWrapper;
    }

    public NettyHandler getNettyHandler() {
        return this.nettyHandler;
    }

    public ConnectionListener getConnectionListener() {
        return this.connectionListener;
    }

    public PacketHandler getPacketHandler() {
        return this.packetHandler;
    }

    public Consumer<Boolean> getConnectingConsumer() {
        return this.connectingConsumer;
    }
}
