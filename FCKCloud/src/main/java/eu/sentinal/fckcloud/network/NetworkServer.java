package eu.sentinal.fckcloud.network;

import eu.sentinal.fckcloud.FCKCloud;
import eu.sentinal.fckcloud.server.pojo.SpigotServer;
import eu.sentinal.fckcloudlib.netty.ConnectionListener;
import eu.sentinal.fckcloudlib.netty.NettyHandler;
import eu.sentinal.fckcloudlib.netty.PacketHandler;
import eu.sentinal.fckcloudlib.netty.packet.Packet;
import eu.sentinal.fckcloudlib.netty.packet.defaults.SetNamePacket;
import eu.sentinal.fckcloudlib.netty.packets.RegisterServerPacket;
import eu.sentinal.fckcloudlib.server.ServerData;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.logging.Logger;

public class NetworkServer {

    public int port = 19132;

    private FCKCloud cloudMaster;

    private NettyHandler nettyHandler;

    private ConnectionListener connectionListener;

    private PacketHandler packetHandler;

    private List<String> whitelist = new ArrayList<>();

    public NetworkServer(FCKCloud cloudMaster, int port, List<String> whitelist) {
        this.cloudMaster = cloudMaster;
        this.port = port;
        this.whitelist = whitelist;

        this.nettyHandler = new NettyHandler();
        this.getNettyHandler().startServer(this.getPort(), success -> {
            if (success) {
                Logger.getGlobal().info("Netty Up and Running!");
            } else {
                Logger.getGlobal().severe("Netty startup failed!");
            }
        });

        this.getNettyHandler().registerConnectionListener(this.connectionListener = new ConnectionListener() {
            @Override
            public void channelConnected(ChannelHandlerContext ctx) {
                String address = ctx.channel().remoteAddress().toString().substring(1).split(":")[0];
                Logger.getGlobal().info("Channel Connection! » " + ctx.channel().remoteAddress());
                try {
                    if (!getWhitelist().contains(address) && !Objects.equals(InetAddress.getLocalHost().getHostAddress(), address)) {
                        ctx.close();
                        return;
                    }
                } catch (UnknownHostException e) {
                    e.printStackTrace();
                    ctx.close();
                    return;
                }

                //getCloudMaster().getServerManager().checkForServers();
            }

            @Override
            public void channelDisconnected(ChannelHandlerContext ctx) {
                String address = ctx.channel().remoteAddress().toString().substring(1).split(":")[0];


                try {
                    if (!getWhitelist().contains(address) && !Objects.equals(InetAddress.getLocalHost().getHostAddress(), address)) {
                        //Logger.getGlobal().warning( "Not Whitelisted IP(" + ctx.channel().remoteAddress().toString().substring( 1 ).split( ":" )[0] + ") want to connect!" );
                        ctx.close();
                        return;
                    }
                } catch (UnknownHostException e) {
                    e.printStackTrace();
                    ctx.close();
                    return;
                }

            }
        });

        this.getNettyHandler().registerPacketHandler(this.packetHandler = new PacketHandler() {
            @Override
            public void incomingPacket(Packet packet, Channel channel) {
                if (packet instanceof SetNamePacket) {
                    SetNamePacket setNamePacket = (SetNamePacket) packet;
                    System.out.println("Got setname » " + setNamePacket.getName());
                    if (getCloudMaster().getServerManager().getServerByName(setNamePacket.getName()) != null) {
                        // Spigot
                        SpigotServer spigotServer = getCloudMaster().getServerManager().getServerByName(setNamePacket.getName());

                        eu.sentinal.fckcloudlib.server.ServerData serverData = new ServerData();
                        serverData.setName(spigotServer.getName());
                        serverData.setMotd(spigotServer.getMotd());
                        serverData.setPort(spigotServer.getPort());
                        serverData.setUniqueId(spigotServer.getUuid());
                        serverData.setHost(spigotServer.getInet());
                        RegisterServerPacket registerServerPacket = new RegisterServerPacket(serverData);
                        Channel target = NettyHandler.getClients().get("Proxy-1");
                        System.out.println(target);
                        Logger.getGlobal().info("Sending register to » " + target.remoteAddress());
                        getPacketHandler().sendPacket(registerServerPacket, target);

                    } else {


                        for (SpigotServer spigotServer : getCloudMaster().getSpigotServers()) {
                            eu.sentinal.fckcloudlib.server.ServerData serverData = new ServerData();
                            serverData.setName(spigotServer.getName());
                            serverData.setMotd(spigotServer.getMotd());
                            serverData.setPort(spigotServer.getPort());
                            serverData.setUniqueId(spigotServer.getUuid());
                            serverData.setHost(spigotServer.getInet());
                            RegisterServerPacket registerServerPacket = new RegisterServerPacket(serverData);
                            getPacketHandler().sendPacket(registerServerPacket, channel);
                        }

                    }
                }
            }


            @Override
            public void registerPackets() {
            }
        });
    }

    public int getPort() {
        return this.port;
    }

    public FCKCloud getCloudMaster() {
        return this.cloudMaster;
    }

    public NettyHandler getNettyHandler() {
        return this.nettyHandler;
    }

    public ConnectionListener getConnectionListener() {
        return this.connectionListener;
    }

    public PacketHandler getPacketHandler() {
        return this.packetHandler;
    }

    public List<String> getWhitelist() {
        return this.whitelist;
    }
}
