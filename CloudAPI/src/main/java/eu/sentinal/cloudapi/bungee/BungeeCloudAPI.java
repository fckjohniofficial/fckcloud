package eu.sentinal.cloudapi.bungee;

import eu.sentinal.cloudapi.bungee.network.Network;
import eu.sentinal.fckcloudlib.config.Config;
import net.md_5.bungee.api.plugin.Plugin;

import java.io.File;
import java.net.InetAddress;
import java.net.UnknownHostException;

public class BungeeCloudAPI extends Plugin {

    private static BungeeCloudAPI instance;

    private Network network;

    private String serverName;

    public BungeeCloudAPI(){
        instance = this;
    }

    public static BungeeCloudAPI getInstance() {
        return BungeeCloudAPI.instance;
    }

    @Override
    public void onEnable() {

        Config dataConfig = new Config( new File( this.getDataFolder(), "data.yml" ), Config.YAML );
        this.serverName = dataConfig.getString("servername");
        try {
            this.network = new Network(this, InetAddress.getLocalHost().getHostAddress(), 8000);
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDisable() {
        getNetwork().getNettyHandler().getNettyClient().disconnect();
    }

    public Network getNetwork() {
        return this.network;
    }

    public String getServerName() {
        return this.serverName;
    }
}
