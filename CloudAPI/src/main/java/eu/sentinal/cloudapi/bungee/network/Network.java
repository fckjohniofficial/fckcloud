package eu.sentinal.cloudapi.bungee.network;

import eu.sentinal.cloudapi.bungee.BungeeCloudAPI;
import eu.sentinal.fckcloudlib.netty.ConnectionListener;
import eu.sentinal.fckcloudlib.netty.NettyHandler;
import eu.sentinal.fckcloudlib.netty.PacketHandler;
import eu.sentinal.fckcloudlib.netty.packet.Packet;
import eu.sentinal.fckcloudlib.netty.packet.defaults.SetNamePacket;
import eu.sentinal.fckcloudlib.netty.packets.DispatchCommandPacket;
import eu.sentinal.fckcloudlib.netty.packets.RegisterServerPacket;
import eu.sentinal.fckcloudlib.netty.packets.UnregisterServerPacket;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.config.ServerInfo;

import java.net.InetSocketAddress;
import java.util.function.Consumer;
import java.util.logging.Logger;

public class Network {

    private String host = "localhost";

    private int port = 19132;

    private BungeeCloudAPI bungeeCloudAPI;

    private NettyHandler nettyHandler;

    private ConnectionListener connectionListener;

    private PacketHandler packetHandler;

    private Consumer<Boolean> connectingConsumer;

    public Network(BungeeCloudAPI bungeeCloudAPI, String host, int port) {
        this.bungeeCloudAPI = bungeeCloudAPI;
        this.host = host;
        this.port = port;

        this.nettyHandler = new NettyHandler();
        this.getNettyHandler().connectToServer( this.getHost(), this.getPort(), this.connectingConsumer = new Consumer<Boolean>() {
            @Override
            public void accept( Boolean success ) {
                if ( success ) {
                    System.out.println("1");
                    Logger.getGlobal().info("Connection to API Successfull");

                    SetNamePacket setNamePacket = new SetNamePacket( Network.this.getBungeeCloudAPI().getServerName() );
                    Network.this.getPacketHandler().sendPacket( setNamePacket );
                } else {
                    Logger.getGlobal().severe("Connection failed");
                    Logger.getGlobal().severe("Attempting reconnect");
                    Network.this.nettyHandler.reconnectToServer( 3, this );
                }
            }
        } );

        this.getNettyHandler().registerConnectionListener(this.connectionListener = new ConnectionListener() {
            @Override
            public void channelConnected(ChannelHandlerContext ctx) {
                Logger.getGlobal().info("Test");
                ProxyServer.getInstance().getLogger().info("Test");
            }

            @Override
            public void channelDisconnected(ChannelHandlerContext ctx) {
                Logger.getGlobal().severe("Lost connection to API");
                Logger.getGlobal().severe("Attempting reconnect");
                Network.this.nettyHandler.reconnectToServer(3, Network.this.getConnectingConsumer());

            }
        });

        this.getNettyHandler().registerPacketHandler(this.packetHandler = new PacketHandler() {
            @Override
            public void incomingPacket(Packet packet, Channel channel) {
                //System.out.println( "Incoming Packet: " + packet.toString() );
                System.out.println("Incoming Packet: " + packet.toString());
                if (packet instanceof RegisterServerPacket) {
                    RegisterServerPacket registerServerPacket = (RegisterServerPacket) packet;
                    System.out.println("Got register!");

                    if (ProxyServer.getInstance().getServers().containsKey(registerServerPacket.getServerData().getName())) {
                        return;
                    }

                    ServerInfo serverInfo = ProxyServer.getInstance().constructServerInfo(
                            registerServerPacket.getServerData().getName(),
                            InetSocketAddress.createUnresolved(registerServerPacket.getServerData().getHost(), registerServerPacket.getServerData().getPort()),
                            registerServerPacket.getServerData().getMotd(),
                            false
                    );
                    ProxyServer.getInstance().getServers().put(registerServerPacket.getServerData().getName(), serverInfo);

                    /*if ( Network.this.getBungeeCloudAPI().getProxyFallbackPriorities().contains( registerServerPacket.getServerData().getServerGroup().getName() ) ) {
                        // Add to fallback Server
                        ProxyServer.getInstance().getConfig().getListeners().iterator().next().getServerPriority().add( registerServerPacket.getServerData().getName() );
                    }
                    */
                    return;
                }

                if (packet instanceof UnregisterServerPacket) {
                    UnregisterServerPacket unregisterServerPacket = (UnregisterServerPacket) packet;

                    if (!ProxyServer.getInstance().getServers().containsKey(unregisterServerPacket.getServerData().getName())) {
                        return;
                    }

                    ProxyServer.getInstance().getServers().remove(unregisterServerPacket.getServerData().getName());
/*
                    if ( Network.this.getBungeeCloudAPI().getProxyFallbackPriorities().contains( unregisterServerPacket.getServerData().getServerGroup().getName() ) ) {
                        // Remove from fallback Server
                        ProxyServer.getInstance().getConfig().getListeners().iterator().next().getServerPriority().remove( unregisterServerPacket.getServerData().getName() );
                    }
                    */
                    return;
                }

                if (packet instanceof DispatchCommandPacket) {
                    DispatchCommandPacket dispatchCommandPacket = (DispatchCommandPacket) packet;
                    ProxyServer.getInstance().getPluginManager().dispatchCommand(ProxyServer.getInstance().getConsole(), dispatchCommandPacket.getCommandline());
                    return;
                }
            }

            @Override
            public void registerPackets() {
            }
        });
    }

    public String getHost() {
        return this.host;
    }

    public int getPort() {
        return this.port;
    }

    public BungeeCloudAPI getBungeeCloudAPI() {
        return this.bungeeCloudAPI;
    }

    public NettyHandler getNettyHandler() {
        return this.nettyHandler;
    }

    public ConnectionListener getConnectionListener() {
        return this.connectionListener;
    }

    public PacketHandler getPacketHandler() {
        return this.packetHandler;
    }

    public Consumer<Boolean> getConnectingConsumer() {
        return this.connectingConsumer;
    }
}
