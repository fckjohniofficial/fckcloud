package eu.sentinal.cloudapi.spigot.network;

import eu.sentinal.cloudapi.spigot.SpigotCloudAPI;
import eu.sentinal.fckcloudlib.netty.ConnectionListener;
import eu.sentinal.fckcloudlib.netty.NettyHandler;
import eu.sentinal.fckcloudlib.netty.PacketHandler;
import eu.sentinal.fckcloudlib.netty.packet.Packet;
import eu.sentinal.fckcloudlib.netty.packet.defaults.SetNamePacket;
import eu.sentinal.fckcloudlib.netty.packets.DispatchCommandPacket;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import org.bukkit.Bukkit;

import java.util.function.Consumer;
import java.util.logging.Logger;

public class Network {

    private String host = "localhost";

    private int port = 19132;

    private SpigotCloudAPI spigotCloudAPI;

    private NettyHandler nettyHandler;

    private ConnectionListener connectionListener;

    private PacketHandler packetHandler;

    private Consumer<Boolean> connectingConsumer;

    public Network( SpigotCloudAPI spigotCloudAPI, String host, int port ) {
        this.spigotCloudAPI = spigotCloudAPI;
        this.host = host;
        this.port = port;

        this.nettyHandler = new NettyHandler();
        this.getNettyHandler().connectToServer( this.getHost(), this.getPort(), this.connectingConsumer = new Consumer<Boolean>() {
            @Override
            public void accept( Boolean success ) {
                if ( success ) {
                    Logger.getGlobal().info("Connection to API Successfull");

                    SetNamePacket setNamePacket = new SetNamePacket( Network.this.getSpigotCloudAPI().getServerName() );
                    Network.this.getPacketHandler().sendPacket( setNamePacket );
                } else {
                    Logger.getGlobal().severe("Connection failed");
                    Logger.getGlobal().severe("Attempting reconnect");
                    Network.this.nettyHandler.reconnectToServer( 3, this );
                }
            }
        } );

        this.getNettyHandler().registerConnectionListener( this.connectionListener = new ConnectionListener() {
            @Override
            public void channelConnected( ChannelHandlerContext ctx ) {
            }

            @Override
            public void channelDisconnected( ChannelHandlerContext ctx ) {
                Logger.getGlobal().severe("Lost connection to API");
                Logger.getGlobal().severe("Attempting reconnect");
                Network.this.nettyHandler.reconnectToServer( 3, Network.this.getConnectingConsumer() );
            }
        } );

        this.getNettyHandler().registerPacketHandler( this.packetHandler = new PacketHandler() {
            @Override
            public void incomingPacket(Packet packet, Channel channel ) {
                if ( packet instanceof DispatchCommandPacket) {
                    DispatchCommandPacket dispatchCommandPacket = (DispatchCommandPacket) packet;
                    Bukkit.dispatchCommand( Bukkit.getConsoleSender(), dispatchCommandPacket.getCommandline() );
                    return;
                }

            }

            @Override
            public void registerPackets() {
            }
        } );
    }

    public String getHost() {
        return this.host;
    }

    public int getPort() {
        return this.port;
    }

    public SpigotCloudAPI getSpigotCloudAPI() {
        return this.spigotCloudAPI;
    }

    public NettyHandler getNettyHandler() {
        return this.nettyHandler;
    }

    public ConnectionListener getConnectionListener() {
        return this.connectionListener;
    }

    public PacketHandler getPacketHandler() {
        return this.packetHandler;
    }

    public Consumer<Boolean> getConnectingConsumer() {
        return this.connectingConsumer;
    }
}
