package eu.sentinal.cloudapi.spigot;

import eu.sentinal.cloudapi.spigot.network.Network;
import eu.sentinal.fckcloudlib.config.Config;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.net.InetAddress;
import java.net.UnknownHostException;

public class SpigotCloudAPI extends JavaPlugin {

    private static SpigotCloudAPI instance;

    private Network network;

    private String serverName;

    public SpigotCloudAPI(){
        instance = this;
    }

    public static SpigotCloudAPI getInstance() {
        return SpigotCloudAPI.instance;
    }

    @Override
    public void onEnable() {
        Config dataConfig = new Config( new File( this.getDataFolder(), "data.yml" ), Config.YAML );
        this.serverName = dataConfig.getString("servername");
        try {
            this.network = new Network(this, InetAddress.getLocalHost().getHostAddress(), 8000);
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onDisable() {
        getNetwork().getNettyHandler().getNettyClient().disconnect();
    }

    public Network getNetwork() {
        return this.network;
    }

    public String getServerName() {
        return this.serverName;
    }
}
